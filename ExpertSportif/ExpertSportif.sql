#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
        IDUtilisateur Int  Auto_increment  NOT NULL ,
        NomU          Char (5) NOT NULL ,
        PrenomU       Char (5) NOT NULL ,
        AdresseU      Varchar (50) NOT NULL ,
        VilleU        Char (5) NOT NULL ,
        StatutU       Varchar (50) NOT NULL ,
        DateU         Datetime NOT NULL ,
        LogU          Varchar (50) NOT NULL ,
        MdpU          Varchar (50) NOT NULL
	,CONSTRAINT Utilisateur_PK PRIMARY KEY (IDUtilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Abonnement
#------------------------------------------------------------

CREATE TABLE Abonnement(
        IdAbo         Int  Auto_increment  NOT NULL ,
        LibAbo        Varchar (50) NOT NULL ,
        DescriptifAbo Text NOT NULL ,
        TarifAbo      Double NOT NULL ,
        DureeAbo      Year NOT NULL
	,CONSTRAINT Abonnement_PK PRIMARY KEY (IdAbo)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Programme
#------------------------------------------------------------

CREATE TABLE Programme(
        IdProg   Int  Auto_increment  NOT NULL ,
        NomProg  Varchar (50) NOT NULL ,
        LienProg Varchar (300) NOT NULL
	,CONSTRAINT Programme_PK PRIMARY KEY (IdProg)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: CoursCol
#------------------------------------------------------------

CREATE TABLE CoursCol(
        IdCours         Int  Auto_increment  NOT NULL ,
        LibCours        Varchar (50) NOT NULL ,
        DescriptifCours Text NOT NULL ,
        TarifCours      Double NOT NULL
	,CONSTRAINT CoursCol_PK PRIMARY KEY (IdCours)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Abonner
#------------------------------------------------------------

CREATE TABLE Abonner(
        IdAbo         Int NOT NULL ,
        IDUtilisateur Int NOT NULL ,
        DateDebut     Datetime NOT NULL ,
        DateFin       Datetime NOT NULL
	,CONSTRAINT Abonner_PK PRIMARY KEY (IdAbo,IDUtilisateur)

	,CONSTRAINT Abonner_Abonnement_FK FOREIGN KEY (IdAbo) REFERENCES Abonnement(IdAbo)
	,CONSTRAINT Abonner_Utilisateur0_FK FOREIGN KEY (IDUtilisateur) REFERENCES Utilisateur(IDUtilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ProgPerso
#------------------------------------------------------------

CREATE TABLE ProgPerso(
        IdProg        Int NOT NULL ,
        IDUtilisateur Int NOT NULL
	,CONSTRAINT ProgPerso_PK PRIMARY KEY (IdProg,IDUtilisateur)

	,CONSTRAINT ProgPerso_Programme_FK FOREIGN KEY (IdProg) REFERENCES Programme(IdProg)
	,CONSTRAINT ProgPerso_Utilisateur0_FK FOREIGN KEY (IDUtilisateur) REFERENCES Utilisateur(IDUtilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Acceder
#------------------------------------------------------------

CREATE TABLE Acceder(
        IdCours Int NOT NULL ,
        IdAbo   Int NOT NULL
	,CONSTRAINT Acceder_PK PRIMARY KEY (IdCours,IdAbo)

	,CONSTRAINT Acceder_CoursCol_FK FOREIGN KEY (IdCours) REFERENCES CoursCol(IdCours)
	,CONSTRAINT Acceder_Abonnement0_FK FOREIGN KEY (IdAbo) REFERENCES Abonnement(IdAbo)
)ENGINE=InnoDB;

