<?php








//VERIFICATION DE LA PRESENCE DE L'UTILISATEUR DANS LA BDD ET ETABLISSEMENT DE LA CONNEXION







if (!isset($_SESSION['utilisateur'])) {

    if(isset($_POST['submitConnex'])){
        if(empty($_POST['login'])){
            $messageErreurConnexion = "Merci de saisir un identifiant de connexion";
        } elseif (empty($_POST['mdp'])){
            $messageErreurConnexion = "Merci de saisir un mot de passe";
        } else{
            if (utilisateurDAO::verification( $_POST['login'], $_POST['mdp']) ){
                $_SESSION['utilisateur'] = utilisateurDAO::utilisateur($_POST['login']);
                header('location: index.php?Menu=accueil');
            }
        }
    } else {
        $messageErreurConnexion='mdp incorrect ou login incorrect';
    }

    if(isset($_POST['submitBtnInscription'])){
        header('location: index.php?Menu=inscription');

    }

    $formulaireConnexion = new Formulaire('post', '', 'fConnexion', 'fConnexion');

    $formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerLabelForBS('inputEmail', 'Connectez vous avec votre adresse Email :','visually-hidden'), 1);
    $formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerInputEmailBS('login', 'form-control', 'login', 'Adresse Mail'));
    $formulaireConnexion->ajouterComposantTab();

    $formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerLabelForBS('mdp', 'Mot de passe :', 'visually-hidden'), 1);
    $formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerInputMdpBS('mdp', 'form-control mb-5','mdp', '' ,'Mot de passe','',''));
    $formulaireConnexion->ajouterComposantTab();

    $formulaireConnexion->ajouterComposantLigne($formulaireConnexion-> creerInputSubmitBS('submitConnex', "w-25 btn btn-lg btn-dark bg-c-blue",'submitConnex', 'Valider'),2);
    $formulaireConnexion->ajouterComposantTab();

    $messageErreurConnexion="";
    $formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerMessage($messageErreurConnexion));
    $formulaireConnexion->ajouterComposantTab();


    $formulaireConnexion->creerFormulaire();




    $formulaireBtnInscription = new Formulaire('post', 'index.php', 'fBtnInscription', 'fBtnInscription');

    $formulaireBtnInscription->ajouterComposantLigne($formulaireBtnInscription->creerInputSubmitBS('submitBtnInscription', "w-25 mb-5 btn btn-lg btn-dark",'submitBtnInscription', 'Inscription'));
    $formulaireBtnInscription->ajouterComposantTab();

    $formulaireBtnInscription->creerFormulaire();


    require_once 'vue/vueConnexion.php' ;


} else {

    header('location: index.php?Menu=moncompte');

}







