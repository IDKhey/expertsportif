<?php



// GESTION INSCRIPTION


if(!isset($_SESSION['utilisateur']) ){

    $message="";

    if(isset($_POST['SubmitInscription'])) {
        if ($_POST['Mdp']==$_POST['ConfirmMdp']) { //Si les deux mot de passe sont identiques
            if (preg_match("#[0][1-6-7][- \.?]?([0-9][0-9][- \.?]?){4}$#", $_POST['Telephone'])) { //Expresssion réguliere pour la confirmation du numero de telephone
                if (!utilisateurDAO::existe($_POST['Mail'])) {

                    //$unNom, $unPrenom, $uneAdresse, $uneVille, $unMail, $unMdp,$unTel
                    if (!utilisateurDAO::inscription($_POST['Nom'], $_POST['Prenom'], $_POST['Adresse'], $_POST['Ville'],$_POST['Mail'],$_POST['Mdp'],$_POST['Telephone'])) {
                        $message = "<h5>Problème lors de la création du compte</h5>";

                    } else {
                        header('location: index.php?Menu=accueil');
                    }
                } else {
                    $message = "<h5>Cette adresse mail ou cet identifiant est déjà utilisé(e) par un autre compte</h5>";
                }
            } else {
                $message = "<h5>Le numéro de telephone n'est pas au bon format</h5>";
            }
        } else{
            $message = "<h5>Erreur les deux mots de passe ne correspondent pas</h5>";
        }
    }



    $formulaireInscription = new Formulaire('post', '', 'fInscription', 'fInscription');



    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Informations personnelles",'visually-hidden'));
    $formulaireInscription->ajouterComposantTab();


    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Nom : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputTexteBS('Nom', 'form-control mb-3','Nom', '', '', 'Nom', ''));
    $formulaireInscription->ajouterComposantTab();

    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Prenom : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputTexteBS('Prenom', 'form-control mb-3 my-auto','Prenom', '', '', 'Prenom', ''));
    $formulaireInscription->ajouterComposantTab();

    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Adresse : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputTexteBS('Adresse', 'form-control mb-3 mb-2','Adresse', '', 1, 'Adresse postale', ''));
    $formulaireInscription->ajouterComposantTab();

    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Ville : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputTexteBS('Ville', 'form-control mb-3','Ville', '', 1, 'Ville', ''));
    $formulaireInscription->ajouterComposantTab();

    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Adresse mail : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputTexteBS('Mail', 'form-control mb-3','Mail', '', '', 'Adresse Mail', ''));
    $formulaireInscription->ajouterComposantTab();

    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Telephone : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputTexteBS('Telephone', 'form-control mb-3','Telephone', '', '', 'N° Telephone', ''));
    $formulaireInscription->ajouterComposantTab();



    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Mot de passe : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputMdpBS('Mdp', 'form-control mb-3','Mdp', '', '', ''));
    $formulaireInscription->ajouterComposantTab();

    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerLabelBS("Confirmation du mot de passe : ",'visually-hidden'));
    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputMdpBS('ConfirmMdp', 'form-control mb-3','ConfirmMdp', '', '', ''));
    $formulaireInscription->ajouterComposantTab();

    $formulaireInscription->ajouterComposantLigne($formulaireInscription->espace());
    $formulaireInscription->ajouterComposantTab();



    $formulaireInscription->ajouterComposantLigne($formulaireInscription->creerInputSubmitBS('SubmitInscription', "w-25 btn btn-lg btn-dark",'SubmitInscription', "Finir l&apos;inscription"));
    $formulaireInscription->ajouterComposantTab();


    $formulaireInscription->creerFormulaire();








    require_once 'vue/vueInscription.php' ;

} else {

    header('location: index.php?Menu=moncompte');

}

