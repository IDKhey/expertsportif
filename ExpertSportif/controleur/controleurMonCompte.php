<?php

$message ='';

// REDIRIGE VERS ACCUEIL SI NON CONNECTE
if(isset($_SESSION['utilisateur'])) {

    //VERIFIE LES CHAMPS + MODIFICATION DES INFORMATIONS

    if (isset($_POST['SubmitModif'])) {

        if (empty($_POST['Nom'])) {
            $message = "Merci de saisir un Nom";
        } elseif (!preg_match("#[0][1-6-7][- \.?]?([0-9][0-9][- \.?]?){4}$#", $_POST['Telephone'])) {
            $message = "Merci de saisir un Numero de Telephone";

        } elseif (!preg_match( "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/", $_POST['Email'])) {
            $message = "L'adresse e-mail n'est pas valide";

        } else {

            utilisateurDAO::modif($_SESSION['utilisateur']->getMailU(), $_POST['Nom'], $_POST['Email'], $_POST['Telephone'],$_POST['Adresse'],$_POST['Ville']);

            $_SESSION['utilisateur'] = utilisateurDAO::utilisateur($_SESSION['utilisateur']->getMailU());

            $message = "Modification Reussie";


        }




    }
        // VERIFICATION DU MDP ACTUEL + MODIFICATION DU MOT DE PASSE

    if (isset($_POST['SubmitModifMdp'])) {

        if (empty($_POST['Mdp'])) {
            $message = "Merci de saisir votre Mot de passe";
        } elseif (empty($_POST['NewMdp'])) {
            $message = "Merci de saisir votre nouveau Mot de Passe";
        } elseif(  utilisateurDAO::verification($_SESSION['utilisateur']->getMailU(), $_POST['Mdp']) == TRUE  ) {


            utilisateurDAO::modifMDP($_SESSION['utilisateur']->getMailU(), $_POST['NewMdp']);

            $_SESSION['utilisateur'] = utilisateurDAO::utilisateur($_SESSION['utilisateur']->getMailU());

            $message = "Modification Reussie";


        }

        else{

            $message = 'Mot de passe actuel erroné';
        }




    }




// CREATION FORMULAIRE PERMETTANT MODIFICATION DU COMPTE

    $formulaireMonCompte = new Formulaire('post', '', 'fMoncompte', 'fMoncompte');


    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->espace());
    $formulaireMonCompte->ajouterComposantTab();


    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerLabelBS("Nom : ","visually-hidden"));
    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerInputTexteBS('Nom', ' form-control','Nom', $_SESSION['utilisateur']->getNomU(), '', '', ''));
    $formulaireMonCompte->ajouterComposantTab();

    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerLabelBS("Prenom : ","visually-hidden"));
    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerInputTexteBS('Prenom', ' form-control','Nom', $_SESSION['utilisateur']->getPrenomU(), '', '', ''));
    $formulaireMonCompte->ajouterComposantTab();

    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerLabelBS("Telephone : ","visually-hidden"));
    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerInputTexteBS('Telephone', ' form-control','Telephone', $_SESSION['utilisateur']->getTelU(), '', '', ''));
    $formulaireMonCompte->ajouterComposantTab();

    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerLabelBS("Email : ","visually-hidden"));
    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerInputTexteBS('Email', ' form-control','Email', $_SESSION['utilisateur']->getMailU(), '', '', ''));
    $formulaireMonCompte->ajouterComposantTab();

    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerLabelBS("Adresse : ","visually-hidden"));
    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerInputTexteBS('Adresse', ' form-control','Adresse', $_SESSION['utilisateur']->getAdresseU(), '', '', ''));
    $formulaireMonCompte->ajouterComposantTab();

    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerLabelBS("Ville : ","visually-hidden"));
    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerInputTexteBS('Ville', ' form-control','Ville', $_SESSION['utilisateur']->getVilleU(), '', '', ''));
    $formulaireMonCompte->ajouterComposantTab();

    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->espace());
    $formulaireMonCompte->ajouterComposantTab();



    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->creerInputSubmitBS('SubmitModif',"w-25 btn btn-s btn-dark",'SubmitModif', "Modifier"));
    $formulaireMonCompte->ajouterComposantTab();


    $formulaireMonCompte->ajouterComposantLigne($formulaireMonCompte->espace());
    $formulaireMonCompte->ajouterComposantTab();

    $formulaireMonCompte->creerFormulaire();






// CREATION FORMULAIRE PERMETTANT MODIFICATION DU MDP

    $formulaireMonMDP = new Formulaire('post', '', 'fMonMDP', 'fMonMDP');


    $formulaireMonMDP->ajouterComposantLigne($formulaireMonMDP->creerLabelBS("Mot de Passe Actuel : ","visually-hidden"));
    $formulaireMonMDP->ajouterComposantLigne($formulaireMonMDP->creerInputMdpBS('Mdp', "form-control",'Mdp', '', 'Mot de passe actuel', ''));
    $formulaireMonMDP->ajouterComposantTab();


    $formulaireMonMDP->ajouterComposantLigne($formulaireMonMDP->creerLabelBS("Nouveau Mot de passe : ","visually-hidden"));
    $formulaireMonMDP->ajouterComposantLigne($formulaireMonMDP->creerInputMdpBS('NewMdp', "form-control",'NewMdp', '', 'Nouveau mot de passe', ''));
    $formulaireMonMDP->ajouterComposantTab();

    $formulaireMonMDP->ajouterComposantLigne($formulaireMonCompte->espace());
    $formulaireMonMDP->ajouterComposantTab();

    $formulaireMonMDP->ajouterComposantLigne($formulaireMonCompte->creerInputSubmitBS('SubmitModifMdp', "w-25 mb-5 btn btn-s btn-dark text-center",'SubmitModifMdp', "Modifier"));
    $formulaireMonMDP->ajouterComposantTab();


    $formulaireMonMDP->creerFormulaire();





    require_once 'vue/vueMonCompte.php';
}

else{

    header('location: index.php?Menu=Connexion');

}


?>