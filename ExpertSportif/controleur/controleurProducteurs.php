<?php

$formulaireProducteur = new Formulaire('post', 'index.php', 'fProducteur', 'fProducteur');
$requete = producteursDAO::recupProducteur();




foreach ($requete as $producteur){


    $formulaireProducteur->ajouterComposantLigne($formulaireProducteur->creerLabelFor('lentreprise',' Nom de l\'entreprise : '. $producteur->getENTREPRISE() ),1);
    $formulaireProducteur->ajouterComposantTab();
    $formulaireProducteur->ajouterComposantLigne($formulaireProducteur->creerLabelFor('ldescriptif',$producteur->getDESCRIPTIF()),1);
    $formulaireProducteur->ajouterComposantTab();
    $formulaireProducteur->ajouterComposantLigne($formulaireProducteur->espace());
    $formulaireProducteur->ajouterComposantLigne($formulaireProducteur->creerLabelFor('ladresse','Adresse : '.$producteur->getADRESSE()),1);
    $formulaireProducteur->ajouterComposantTab();
    $formulaireProducteur->ajouterComposantLigne($formulaireProducteur->espace());
    $formulaireProducteur->ajouterComposantTab();

    $formulaireProducteur->creerFormulaire();
}


require_once 'vue/vueProducteurs.php' ;
