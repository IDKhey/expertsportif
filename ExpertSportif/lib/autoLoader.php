<?php
spl_autoload_register('Autoloader::autoloadDTO');
spl_autoload_register('Autoloader::autoloadDAO');
spl_autoload_register('Autoloader::autoloadLib');
spl_autoload_register('Autoloader::autoloadTraits');


class Autoloader{
    
     
    
    static function autoloadDTO($class){
        $file = 'modeles/DTO/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
      
    }
    
    static function autoloadLib($class){
        $file = 'lib/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
        
    }
    
    static function autoloadDAO($class){
        $file = 'modeles/DAO/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
        
    }
    static function autoloadTraits($class){
        $file = 'modeles/traits/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
        
    }

    
    
}


