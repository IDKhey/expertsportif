<?php
class Formulaire{
	private $method;
	private $action;
	private $nom;
	private $style;
	private $formulaireToPrint;
	
	private $ligneComposants = array();
	private $tabComposants = array();
	
	public function __construct($uneMethode, $uneAction , $unNom,$unStyle ){
		$this->method = $uneMethode;
		$this->action =$uneAction;
		$this->nom = $unNom;
		$this->style = $unStyle;
	}
	
	
	public function concactComposants($unComposant , $autreComposant ){
		$unComposant .=  $autreComposant;
		return $unComposant ;
	}
	
	public function ajouterComposantLigne($unComposant){
		$this->ligneComposants[] = $unComposant;
	}
	
	public function ajouterComposantTab(){
		$this->tabComposants[] = $this->ligneComposants;
		$this->ligneComposants = array();
	}
	
	public function creerLabel($unLabel){
		$composant = "<label  >" . $unLabel . "</label>";
		return $composant;
	}


    public function creerLabelBS($unLabel,$uneClass){
        $composant = "<label class = '" . $uneClass . "' >" . $unLabel . "</label>";
        return $composant;
    }
	
	public function creerMessage($unMessage){
		$composant = "<label class='message'>" . $unMessage . "</label>";
		return $composant;
	}


	
	public function creerInputTexte($unNom, $unId, $uneValue , $required , $placeholder , $pattern){
		$composant = "<input type = 'text' name = '" . $unNom . "' id = '" . $unId . "' ";
		if (!empty($uneValue)){
			$composant .= "value = '" . $uneValue . "' ";
		}
		if (!empty($placeholder)){
			$composant .= "placeholder = '" . $placeholder . "' ";
		}
		if ( $required = 1){
			$composant .= "required ";
		}
		if (!empty($pattern)){
			$composant .= "pattern = '" . $pattern . "' ";
		}
		$composant .= "/>";
		return $composant;
	}

    public function creerInputTexteBS($unNom, $uneClass, $unId, $uneValue , $required , $placeholder , $pattern){
        $composant = "<input type = 'text' name = '" . $unNom . "' class = '" . $uneClass . "' id = '" . $unId . "' ";
        if (!empty($uneValue)){
            $composant .= "value = '" . $uneValue . "' ";
        }
        if (!empty($placeholder)){
            $composant .= "placeholder = '" . $placeholder . "' ";
        }
        if ( $required = 1){
            $composant .= "required ";
        }
        if (!empty($pattern)){
            $composant .= "pattern = '" . $pattern . "' ";
        }
        $composant .= "/>";
        return $composant;
    }
	
	
	public function creerInputMdp($unNom, $unId,  $required , $placeholder , $pattern){
		$composant = "<input type = 'password' name = '" . $unNom . "' id = '" . $unId . "' ";
		if (!empty($placeholder)){
			$composant .= "placeholder = '" . $placeholder . "' ";
		}
		if ( $required = 1){
			$composant .= "required ";
		}
		if (!empty($pattern)){
			$composant .= "pattern = '" . $pattern . "' ";
		}
		$composant .= "/>";
		return $composant;
	}

    public function creerInputMdpBS($unNom, $uneClass, $unId, $required , $placeholder , $pattern){
        $composant = "<input type = 'password' name = '" . $unNom . "' class = '" . $uneClass . "' id = '" . $unId . "' ";
        if (!empty($placeholder)){
            $composant .= "placeholder = '" . $placeholder . "' ";
        }
        if ( $required = 1){
            $composant .= "required ";
        }
        if (!empty($pattern)){
            $composant .= "pattern = '" . $pattern . "' ";
        }
        $composant .= "/>";
        return $composant;
    }


    public function creerLabelFor($unFor,  $unLabel){
		$composant = "<label for='" . $unFor . "'>" . $unLabel . "</label>";
		return $composant;

	}

    public function creerLabelForBS($unFor,  $unLabel, $uneClass){
        $composant = "<label for='" . $unFor . "' class='" . $uneClass . "'>" . $unLabel . "</label>";
        return $composant;

    }

	public function creerSelect($unNom, $unId, $unLabel, $options){
		$composant = "<select  name = '" . $unNom . "' id = '" . $unId . "' >";
		foreach ($options as $option){
			$composant .= "<option value ='".$option."'>".$option."</option>" ;
		}
		$composant .= "</select></td></tr>";
		return $composant;
	}	
	
	public function creerInputSubmit($unNom, $unId, $uneValue){
		$composant = "<input type = 'submit' name = '" . $unNom . "' id = '" . $unId . "' ";
		$composant .= "value = '" . $uneValue . "'/> ";
		return $composant;
	}


    public function creerInputSubmitBS($unNom, $uneClass, $unId, $uneValue){
        $composant = "<input type = 'submit' name = '" . $unNom . "' class = '" . $uneClass . "' id = '" . $unId . "' ";
        $composant .= "value = '" . $uneValue . "'/> ";
        return $composant;
    }

    public function creerInputEmailBS($unNom, $uneClass, $unId, $placeholder){
        $composant = "<input type = 'email' name = '" . $unNom . "' class = '" . $uneClass . "' id = '" . $unId . "' ";
        if (!empty($placeholder)){
            $composant .= "placeholder = '" . $placeholder . "' ";
        }
        return $composant;
    }

    public function creerInputImage($unNom, $unId, $uneSource){
		$composant = "<input type = 'image' name = '" . $unNom . "' id = '" . $unId . "' ";
		$composant .= "src = '" . $uneSource . "'/> ";
		return $composant;
	}
	
	
	public function creerFormulaire(){
		$this->formulaireToPrint = "<form method = '" .  $this->method . "' ";
		$this->formulaireToPrint .= "action = '" .  $this->action . "' ";
		$this->formulaireToPrint .= "name = '" .  $this->nom . "' ";
		$this->formulaireToPrint .= "class = '" .  $this->style . "' >";
		
	
		foreach ($this->tabComposants as $uneLigneComposants){
			$this->formulaireToPrint .= "<div class = 'ligne'>";
			foreach ($uneLigneComposants as $unComposant){
				$this->formulaireToPrint .= $unComposant ;
			}
			$this->formulaireToPrint .= "</div>";
		}
		$this->formulaireToPrint .= "</form>";
		return $this->formulaireToPrint ;
	}
	
	public function afficherFormulaire(){
		echo $this->formulaireToPrint ;
	}

    /**********************************************************************************************
     *******************************Personnel****************************************************
     **********************************************************************************************/

    public function creerListeDeroulante($liste, $unId,$isAbo=false){
        $composant = "<select id = '".$unId."'>";
        $composant .= "<choice/>";
        $composant .= "<mandatory/>";


        //PERMET DE PARCOURIR LE TABLEAU D'ABONNEMENT (car Tableau dans Tableau)

        if($isAbo==true){

            foreach($liste as $list){

                $composant .= "<option value='".$list['CODEFORMULE']. "'>".$list['LIBELLEFORMULE']."</option>";

            }


        }
        //SINON CREER UNE LISTE DEROULANTE NORMALE (car tableau normal)

        else{	for ($i=0 ; $i < count($liste) ; $i++) {
            $composant .= "<option value='".$liste[$i]."'>".$liste[$i]."</option>";
        }


        }

        $composant.= "</select>";
        return $composant;
    }




    public function creerCheckBoxDeroulante($liste, $unId){
        $composant="";
        for ($i=0 ; $i < count($liste) ; $i++) {
            $composant .= "<div>";
            $composant .= "<input type='checkbox' id='".$liste[$i]."'/>";
            $composant .= $this->creerLabelFor($liste[$i], $liste[$i]);
            $composant .= "</div>";
        }
        return $composant;
    }






    public function creerInputDate($unNom){
        $today = date('d-m-Y');
        $composant = "<input type='date' name = '" . $unNom . "' min = '" . $today . "' id = '" . $unNom . "' >  </input>";
        return $composant;
    }

    public function creerInputTime($unNom){

        $composant = "<input type='time' name = '" . $unNom . "' id = '" . $unNom . "' >  </input>";
        return $composant;
    }

    public function creerInputNumber($unNom,$unMax){

        $composant = "<input type='number' min='0' max=$unMax name = '" . $unNom . "' id = '" . $unNom . "' >  </input>";
        return $composant;
    }

    public function espace(){
        $composant = "</br>";
        return $composant;
    }


    public function creerTitre($unTitre){
        $composant = "<h1 class='titre_perso'>" . $unTitre . "</h1>";
        return $composant;
    }

    public function creerInputRadio($unNom, $unId, $uneValue , $required){
        $composant = "<input type = 'radio' name = '" . $unNom . "' id = '" . $unId . "' ";
        if (!empty($uneValue)){
            $composant .= "value = '" . $uneValue . "' ";
        }
        if ( $required = 1){
            $composant .= "required ";
        }
        $composant .= "/>";
        return $composant;
    }



    public function creerInputTexteGris($unNom, $unId, $uneValue , $required , $placeholder , $pattern){
        $composant = "<input type = 'text' readonly ='readonly' name = '" . $unNom . "' id = '" . $unId . "' ";
        if (!empty($uneValue)){
            $composant .= "value = '" . $uneValue . "' ";
        }
        if (!empty($placeholder)){
            $composant .= "placeholder = '" . $placeholder . "' ";
        }
        if ( $required = 1){
            $composant .= "required ";
        }
        if (!empty($pattern)){
            $composant .= "pattern = '" . $pattern . "' ";
        }
        $composant .= "/>";
        return $composant;
    }










}