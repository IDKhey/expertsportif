<?php
class AbonnementDAO
{
    use Hydrate;


    public static function recupAbo(){

        $results = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from Abonnement ");
        $requetePrepa->execute();
        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $abonnement){


            $unAbo = new Abonnement();

            $unAbo->hydrate($abonnement);

            $results[]= $unAbo;

        }

        return $results;

    }

    public static function recupAboPrincipal(){

        $results = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from Abonnement where idAbo = '3' OR idAbo='6'");
        $requetePrepa->execute();
        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $abonnement){


            $unAbo = new Abonnement();

            $unAbo->hydrate($abonnement);

            $results[]= $unAbo;

        }

        return $results;

    }

    public static function recupTousLesAbosAvecLesTarifs() {
        $abonnements = abonnementDAO::recupAbo();
        $aboToDisplay = array();

        // Pour chaque abonnements
        foreach ($abonnements as $abo){
            // Recuperation des tarifs liées à l'abonnement
            $tarifsAbonnement = tarifAbonnementDAO::recupTarifAboByAboId($abo->getIdAbo());

            // On parcours la liste des tarifs de l'abonnemnt
            // Un Abonnement peut avoir plusieur tarif

            // ON verifie que l'abonnement dispose d'un tarif
            if(!empty( $tarifsAbonnement)){
                foreach($tarifsAbonnement as $tarifAbo){
                    // Creation d'un tableau qui contient les tarifs des l'abonnement
                    $tarifs = array();
                    // Pour chaque tarif d'abonnement on recupère le libellé et son montant
                    $tarifs[] = tarifDAO::recupTarifById($tarifAbo->getIdTarif());
                    foreach($tarifs as $tarif) {
                        $abo->tarifs[] = ["montantTarif" =>  $tarif[0]->getMontantTarif(), "libTarif" => $tarif[0]->getLibTarif()];
                    }
                }
                $aboToDisplay[] = $abo;
            }

        }
        return $aboToDisplay;
    }

    public static function recupAboPrincipalAvecLesTarifs() {
        $abonnements = abonnementDAO::recupAboPrincipal();
        $aboToDisplay = array();

        // Pour chaque abonnements
        foreach ($abonnements as $abo){
            // Recuperation des tarifs liées à l'abonnement
            $tarifsAbonnement = tarifAbonnementDAO::recupTarifAboByAboId($abo->getIdAbo());

            // On parcours la liste des tarifs de l'abonnemnt
            // Un Abonnement peut avoir plusieur tarif

            // ON verifie que l'abonnement dispose d'un tarif
            if(!empty( $tarifsAbonnement)){
                foreach($tarifsAbonnement as $tarifAbo){
                    // Creation d'un tableau qui contient les tarifs des l'abonnement
                    $tarifs = array();
                    // Pour chaque tarif d'abonnement on recupère le libellé et son montant
                    $tarifs[] = tarifDAO::recupTarifById($tarifAbo->getIdTarif());
                    foreach($tarifs as $tarif) {
                        $abo->tarifs[] = ["montantTarif" =>  $tarif[0]->getMontantTarif(), "libTarif" => $tarif[0]->getLibTarif()];
                    }
                }
                $aboToDisplay[] = $abo;
            }

        }
        return $aboToDisplay;
    }


}