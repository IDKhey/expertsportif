<?php
class panierDAO
{
    use Hydrate;



    public static function ajouterPanier($idProduit, $quantite){

        $requetePrepa = "";
        $idadherant = $_SESSION['utilisateur']->getIDADHERANT();

        if (panierDAO::existe($idProduit)) {
            $requetePrepa = "UPDATE PANIER  SET QUANTITE = QUANTITE + :qte  WHERE IDADHERANT = :idadherant  AND IDPRODUIT = :idproduit";
        } else {
            $requetePrepa = "INSERT INTO PANIER VALUES (:idadherant, :idproduit, :qte)";
        }

        $requetePrepa = DBConnex::getInstance()->prepare($requetePrepa);


        $requetePrepa->bindParam(":idproduit", $idProduit);
        $requetePrepa->bindParam(":qte", $quantite);
        $requetePrepa->bindParam(":idadherant", $idadherant);

        return $requetePrepa->execute();
    }




    public static function existe($numProduit){

        $idadherant = $_SESSION['utilisateur']->getIDADHERANT();
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM PANIER WHERE IDADHERANT = :idadherant  AND IDPRODUIT = :idproduit");
        $requetePrepa->bindParam(":idproduit", $numProduit);
        $requetePrepa->bindParam(":idadherant", $idadherant);

        $requetePrepa->execute();


        $liste = $requetePrepa->fetchAll();

        if (is_null($liste) || empty($liste)) {
            return false;
        } else {
            return true;
        }
    }


    public static function recupPanier(){

        $idadherant = $_SESSION['utilisateur']->getIDADHERANT();
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from PANIER WHERE :id = IDADHERANT");
        $requetePrepa->bindParam(":id", $idadherant);
        $requetePrepa->execute();

        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $produit){


            $unProduit = new PANIER();

            $unProduit->hydrate($produit);

            $result[]= $unProduit;



        }

        return $result;

    }



    public static function recupProduit($unID){

        //$result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select pr.IDPRODUIT as IDPRODUIT, 
                                                                    pr.NOMPRODUIT AS NOMPRODUIT, 
                                                                    pr.DESCRIPTIFPRODUIT AS DESCRIPTIFPRODUIT, 
                                                                    pr.UNITE AS UNITE, 
                                                                    p.QUANTITE AS QUANTITEPRODUIT, 
                                                                    pp.PRIXPRODUIT AS PRIXPRODUIT,
                                                                    pp.QUANTITEPRODUIT AS QUANTITEMAX
                                                                    FROM produits as pr, PANIER as p, produit_proposer as pp
                                                                    WHERE pp.IDPRODUIT = pr.IDPRODUIT
                                                                    AND pp.IDPRODUIT = p.IDPRODUIT
                                                                    AND p.IDPRODUIT = :idproduit;
                                                                    ");

        $requetePrepa->bindParam(":idproduit", $unID);
        $requetePrepa->execute();

        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $produit){


            $unProduit = new produits();

            $unProduit->hydrate($produit);

            $result[]= $unProduit;

        }

        return $requete;

    }


    public static function ViderPanier(){

        $requetePrepa = "";
        $idadherant = $_SESSION['utilisateur']->getIDADHERANT();

        $requetePrepa = "DELETE FROM PANIER WHERE IDADHERANT = :idadherant ";


        $requetePrepa = DBConnex::getInstance()->prepare($requetePrepa);

        $requetePrepa->bindParam(":idadherant", $idadherant);

        return $requetePrepa->execute();
    }


}
