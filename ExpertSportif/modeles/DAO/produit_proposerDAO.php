<?php
class Produit_proposerDAO
{
    use Hydrate;


    public static function recupProduitProposer(){

        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from produits_proposer ");
        $requetePrepa->execute();

        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $produit){


            $unProduit = new produits_proposer();

            $unProduit->hydrate($produit);

            $result[]= $unProduit;

        }

        return $result;

    }


public static function produitProposer($unProduit){

       // var_dump($unProduit);
       // die;
        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM produit_proposer WHERE IDPRODUIT = :idproduit
                                                                        AND IDSEMAINE = (SELECT MAX(IDSEMAINE FROM produit_proposer))");
        $requetePrepa->bindParam(":idproduit", $unProduit);
        $requetePrepa->execute();


    $requete = $requetePrepa->fetch();

    if(!empty($requete)){

        $unProduit = new produit_proposer;
        $unProduit->hydrate($requete);
        return $unProduit;

    }

}



}