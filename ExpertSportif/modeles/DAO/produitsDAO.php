<?php
class ProduitsDAO
{
    use Hydrate;


    public static function recupProduit(){

        //$result = [];
        // $requetePrepa = DBConnex::getInstance()->prepare("select * from produits as p, produit_proposer as pp, semaine as s WHERE p.IDPRODUIT = pp.IDPRODUIT AND pp.IDSEMAINE=(SELECT MAX(s.IDSEMAINE) from semaine as s)");
        $requetePrepa = DBConnex::getInstance()->prepare("select p.IDPRODUIT as IDPRODUIT, p.NOMPRODUIT AS NOMPRODUIT, p.DESCRIPTIFPRODUIT AS DESCRIPTIFPRODUIT, p.UNITE AS UNITE, pp.QUANTITEPRODUIT AS QUANTITEPRODUIT, pp.PRIXPRODUIT AS PRIXPRODUIT, pp.IDSEMAINE AS IDSEMAINE  from produit_proposer as pp, produits as p WHERE pp.IDPRODUIT = p.IDPRODUIT AND pp.IDSEMAINE=(SELECT MAX(IDSEMAINE) from semaine)");

        $requetePrepa->execute();

        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $produit){


           $unProduit = new produits();

          $unProduit->hydrate($produit);

          $result[]= $unProduit;

        }

        return $requete;

    }



    /*public static function recupProduit(){

        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from produits WHERE ");
        $requetePrepa->execute();

        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $produit){


            $unProduit = new produits();

            $unProduit->hydrate($produit);

            $result[]= $unProduit;

        }

        return $result;

    }
*/



}