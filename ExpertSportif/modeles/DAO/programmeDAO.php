<?php


class programmeDAO
{
    use Hydrate;

    public static function recupProgramme($idProg){


        $requetePrepa = DBConnex::getInstance()->prepare("Select * from Programme where IdProg = :idprog");
        $requetePrepa->bindParam( ":idprog", $idProg);
        $requetePrepa->execute();
        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        $result = array();

        foreach ($requete as $programme){
            $unProgramme = new programme();
            $unProgramme->hydrate($programme);
            $result[]= $unProgramme;
        }

        return $result;
    }

    //JE DOIS RECUPERER L'ID De l'abonne, puis recuperer tout les progPerso ayant pour IDAbo celui recupere
    // ENSUITE pour chaque progPerso ayant pour IdAbo celui de l'abonne Je dois recuperer le lien du programme


    public static function recupMesProgPerso(){
        
        $idUtilisateur = $_SESSION['utilisateur']->getIDUtilisateur();
        $idProg = progPersoDAO::recupMesIdProgrammes($idUtilisateur);
        
        /*$requetePrepa = DBConnex::getInstance()->prepare("Select * from Programme where IdProg = :idprog");
        $requetePrepa->bindParam( ":idprog", $idProg);
        $requetePrepa->execute();
        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);*/

        $result = array();

        foreach ($idProg as $unId){

            $result[]= self::recupProgramme($unId["idProg"]);
        }


        return $result;

    }
        
    


}