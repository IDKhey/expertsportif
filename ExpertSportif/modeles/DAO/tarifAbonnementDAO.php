<?php
class tarifAbonnementDAO
{
    use Hydrate;


    public static function recupTarifAboByAboId(int $idAbo){

        $results = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select idTarif from TarifAbonnement where idAbo = :idAbo");
        $requetePrepa->bindParam(":idAbo" , $idAbo);
        $requetePrepa->execute();
        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $idTarifAbo){

            $unIdTarifAbo = new TarifAbonnement();

            $unIdTarifAbo->hydrate($idTarifAbo);

            $results[] = $unIdTarifAbo;

        }

        return $results;

    }

}