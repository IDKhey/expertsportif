<?php
class tarifDAO
{
    use Hydrate;


    public static function recupTarifById(int $idTarif){

        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from Tarif where idTarif = :idTarif");
        $requetePrepa->bindParam(":idTarif" , $idTarif);
        $requetePrepa->execute();
        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $tarif){

            $unTarif = new Tarif();

            $unTarif->hydrate($tarif);

            $result[]= $unTarif;

        }

        return $result;

    }

}