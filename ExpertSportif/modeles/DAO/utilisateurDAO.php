<?php
class utilisateurDAO{
    use Hydrate;




    public static function utilisateur($login){

        $requetePrepa = DBConnex::getInstance()->prepare("select *  from Utilisateur where MailU = :login");

        $requetePrepa->bindParam( ":login", $login);

        $requetePrepa->execute();

        $requete = $requetePrepa->fetch();
     
        if(!empty($requete)){

            $unUtilisateur = new Utilisateur();
            $unUtilisateur->hydrate($requete);
            return $unUtilisateur;

        }


    }


    public static function recupUtilisateur(){

        $requetePrepa = DBConnex::getInstance()->prepare("select * from Utilisateur ");


        $requetePrepa->execute();

        $requete = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        foreach ($requete as $user){


            $unUtilisateur = new Utilisateur();

            $unUtilisateur->hydrate($user);

            $result[]= $unUtilisateur;

        }

        return $requete;

    }





    public static function modif($login, $nom, $email, $tel,$adresse,$ville) {
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE Utilisateur SET NomU = :nom, MailU = :mail, TelU = :tel, AdresseU = :adresse, VilleU = :ville WHERE MailU = :login");
        $requetePrepa->bindParam(":nom" , $nom);
        $requetePrepa->bindParam(":mail", $email);
        $requetePrepa->bindParam(":tel", $tel);
        $requetePrepa->bindParam(":login", $login);
        $requetePrepa->bindParam(":adresse", $adresse);
        $requetePrepa->bindParam(":ville", $ville);

        $result = $requetePrepa->execute();
        return $result;
    }


    public static function verification($unLogin,$unPass){

        $requetePrepa = DBConnex::getInstance()->prepare("select *  from Utilisateur where MailU = :login and  MdpU = md5(:mdp)");
        $login = $unLogin;
        $mdp = $unPass;
        $requetePrepa->bindParam( ":login", $login);
        $requetePrepa->bindParam( ":mdp" ,  $mdp);
        $requetePrepa->execute();

        if(empty($requetePrepa->fetch())){
            return false;


        }
        else{
            return TRUE;
        }


    }





    public static function modifMDP($login, $mdp) {
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE Utilisateur SET MdpU = md5(:mdp) WHERE MailU = :login");
        $requetePrepa->bindParam(":mdp" , $mdp);
        $requetePrepa->bindParam(":login", $login);


        $result = $requetePrepa->execute();
        return $result;
    }


    public static function inscription($unNom, $unPrenom, $uneAdresse, $uneVille, $unMail, $unMdp,$unTel){


        $requetePrepa = DBConnex::getInstance()->prepare('INSERT INTO `Utilisateur` ( `NomU`, `PrenomU`, `AdresseU`, `VilleU`, `DateU`, `MailU`, `MdpU`,`TelU`) VALUES (:nom,:prenom,:adresse,:ville,:dateU ,:mail,md5(:mdp),:tel)');



        $date = date('Y-m-d');

        $requetePrepa->bindParam( ":nom" ,  $unNom);
        $requetePrepa->bindParam( ":prenom", $unPrenom);
        $requetePrepa->bindParam( ":adresse", $uneAdresse);
        $requetePrepa->bindParam( ":ville", $uneVille);
        $requetePrepa->bindParam( ":dateU" ,  $date);
        $requetePrepa->bindParam( ":mail", $unMail);
        $requetePrepa->bindParam( ":mdp" ,  $unMdp);
        $requetePrepa->bindParam( ":tel" ,  $unTel);






        //$requetePrepa->debugDumpParams();



        return $requetePrepa->execute();;

    }

   public static function verifAdmin(){

        $utilisateurConnecte = $_SESSION['utilisateur'];
        if(!isset($utilisateurConnecte)){
            return false;
        }

        $role = $utilisateurConnecte->getStatutU();
        if($role == "admin"){
            return true;
        }

        return false;
    }

    public static function existe($unMail)
    {
        $existe = false;

        $requetePrepa = DBConnex::getInstance()->prepare("SELECT * FROM Utilisateur WHERE MailU = :mail");
        $requetePrepa->bindParam(":mail", $unMail);
        $requetePrepa->execute();

        if (!empty($requetePrepa->fetch())) {
            $existe = true;
        }

        return $existe;
    }

}
