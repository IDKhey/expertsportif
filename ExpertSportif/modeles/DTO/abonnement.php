<?php
class Abonnement{
    use hydrate;
    private $IdAbo;
   private $LibAbo;
   private $DescriptifAbo;

    /**
     * @return mixed
     */
    public function getIdAbo()
    {
        return $this->IdAbo;
    }

    /**
     * @param mixed $IdAbo
     */
    public function setIdAbo($IdAbo): void
    {
        $this->IdAbo = $IdAbo;
    }

    /**
     * @return mixed
     */
    public function getLibAbo()
    {
        return $this->LibAbo;
    }

    /**
     * @param mixed $LibAbo
     */
    public function setLibAbo($LibAbo): void
    {
        $this->LibAbo = $LibAbo;
    }

    /**
     * @return mixed
     */
    public function getDescriptifAbo()
    {
        return $this->DescriptifAbo;
    }

    /**
     * @param mixed $DescriptifAbo
     */
    public function setDescriptifAbo($DescriptifAbo): void
    {
        $this->DescriptifAbo = $DescriptifAbo;
    }

  



}
