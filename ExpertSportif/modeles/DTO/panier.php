<?php
class PANIER
{
    use hydrate;

    private $IDPRODUIT;
    private $IDADHERANT;
    private $QUANTITE;

    /**
     * @return mixed
     */
    public function getIDPRODUIT()
    {
        return $this->IDPRODUIT;
    }

    /**
     * @param mixed $IDPRODUIT
     */
    public function setIDPRODUIT($IDPRODUIT): void
    {
        $this->IDPRODUIT = $IDPRODUIT;
    }

    /**
     * @return mixed
     */
    public function getIDADHERANT()
    {
        return $this->IDADHERANT;
    }

    /**
     * @param mixed $IDADHERANT
     */
    public function setIDADHERANT($IDADHERANT): void
    {
        $this->IDADHERANT = $IDADHERANT;
    }

    /**
     * @return mixed
     */
    public function getQUANTITE()
    {
        return $this->QUANTITE;
    }

    /**
     * @param mixed $QUANTITE
     */
    public function setQUANTITE($QUANTITE): void
    {
        $this->QUANTITE = $QUANTITE;
    }


}