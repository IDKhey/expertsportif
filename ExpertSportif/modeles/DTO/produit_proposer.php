<?php
class produit_proposer
{
    use hydrate;

    private $IDSEMAINE;
    private $IDPRODUIT;
    private $QUANTITEPRODUIT;
    private $PRIXPRODUIT;

    /**
     * @return mixed
     */
    public function getIDSEMAINE()
    {
        return $this->IDSEMAINE;
    }

    /**
     * @param mixed $IDSEMAINE
     */
    public function setIDSEMAINE($IDSEMAINE): void
    {
        $this->IDSEMAINE = $IDSEMAINE;
    }

    /**
     * @return mixed
     */
    public function getIDPRODUIT()
    {
        return $this->IDPRODUIT;
    }

    /**
     * @param mixed $IDPRODUIT
     */
    public function setIDPRODUIT($IDPRODUIT): void
    {
        $this->IDPRODUIT = $IDPRODUIT;
    }

    /**
     * @return mixed
     */
    public function getQUANTITEPRODUIT()
    {
        return $this->QUANTITEPRODUIT;
    }

    /**
     * @param mixed $QUANTITEPRODUIT
     */
    public function setQUANTITEPRODUIT($QUANTITEPRODUIT): void
    {
        $this->QUANTITEPRODUIT = $QUANTITEPRODUIT;
    }

    /**
     * @return mixed
     */
    public function getPRIXPRODUIT()
    {
        return $this->PRIXPRODUIT;
    }

    /**
     * @param mixed $PRIXPRODUIT
     */
    public function setPRIXPRODUIT($PRIXPRODUIT): void
    {
        $this->PRIXPRODUIT = $PRIXPRODUIT;
    }

}