<?php
class produits
{ use hydrate;
    private $IDPRODUIT;
    private $NOMPRODUIT;
    private $DESCRIPTIFPRODUIT;
    private $IDPRODUCTEUR;
    private $CODECATEGORIE;
    private $UNITE;

    /**
     * @return mixed
     */
    public function getIDPRODUIT()
    {
        return $this->IDPRODUIT;
    }

    /**
     * @param mixed $IDPRODUIT
     */
    public function setIDPRODUIT($IDPRODUIT): void
    {
        $this->IDPRODUIT = $IDPRODUIT;
    }

    /**
     * @return mixed
     */
    public function getNOMPRODUIT()
    {
        return $this->NOMPRODUIT;
    }

    /**
     * @param mixed $NOMPRODUIT
     */
    public function setNOMPRODUIT($NOMPRODUIT): void
    {
        $this->NOMPRODUIT = $NOMPRODUIT;
    }

    /**
     * @return mixed
     */
    public function getDESCRIPTIFPRODUIT()
    {
        return $this->DESCRIPTIFPRODUIT;
    }

    /**
     * @param mixed $DESCRIPTIFPRODUIT
     */
    public function setDESCRIPTIFPRODUIT($DESCRIPTIFPRODUIT): void
    {
        $this->DESCRIPTIFPRODUIT = $DESCRIPTIFPRODUIT;
    }

    /**
     * @return mixed
     */
    public function getIDPRODUCTEUR()
    {
        return $this->IDPRODUCTEUR;
    }

    /**
     * @param mixed $IDPRODUCTEUR
     */
    public function setIDPRODUCTEUR($IDPRODUCTEUR): void
    {
        $this->IDPRODUCTEUR = $IDPRODUCTEUR;
    }

    /**
     * @return mixed
     */
    public function getCODECATEGORIE()
    {
        return $this->CODECATEGORIE;
    }

    /**
     * @param mixed $CODECATEGORIE
     */
    public function setCODECATEGORIE($CODECATEGORIE): void
    {
        $this->CODECATEGORIE = $CODECATEGORIE;
    }

    /**
     * @return mixed
     */
    public function getUNITE()
    {
        return $this->UNITE;
    }

    /**
     * @param mixed $UNITE
     */
    public function setUNITE($UNITE): void
    {
        $this->UNITE = $UNITE;
    }



}


