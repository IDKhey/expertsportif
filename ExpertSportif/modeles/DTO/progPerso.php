<?php
class progPerso{

    use hydrate;

    private $IdProg;
    private $IDUtilisateur;

    /**
     * @return mixed
     */
    public function getIdProg()
    {
        return $this->IdProg;
    }

    /**
     * @param mixed $IdProg
     */
    public function setIdProg($IdProg): void
    {
        $this->IdProg = $IdProg;
    }

    /**
     * @return mixed
     */
    public function getIDUtilisateur()
    {
        return $this->IDUtilisateur;
    }

    /**
     * @param mixed $IDUtilisateur
     */
    public function setIDUtilisateur($IDUtilisateur): void
    {
        $this->IDUtilisateur = $IDUtilisateur;
    }




}


