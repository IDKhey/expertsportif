<?php
class programme{

    use hydrate;

    private $IdProg;
    private $NomProg;
    private $LienProg;

    /**
     * @return mixed
     */
    public function getIdProg()
    {
        return $this->IdProg;
    }

    /**
     * @param mixed $IdProg
     */
    public function setIdProg($IdProg): void
    {
        $this->IdProg = $IdProg;
    }

    /**
     * @return mixed
     */
    public function getNomProg()
    {
        return $this->NomProg;
    }

    /**
     * @param mixed $NomProg
     */
    public function setNomProg($NomProg): void
    {
        $this->NomProg = $NomProg;
    }

    /**
     * @return mixed
     */
    public function getLienProg()
    {
        return $this->LienProg;
    }

    /**
     * @param mixed $LienProg
     */
    public function setLienProg($LienProg): void
    {
        $this->LienProg = $LienProg;
    }


}


