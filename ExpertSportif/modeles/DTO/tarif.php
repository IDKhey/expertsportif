<?php
class Tarif{
    use hydrate;

   private $idTarif;
   private $libTarif;
   private $montantTarif;

    /**
     * @return mixed
     */
    public function getIdTarif()
    {
        return $this->idTarif;
    }

    /**
     * @param mixed $idTarif
     */
    public function setIdTarif($idTarif): void
    {
        $this->idTarif = $idTarif;
    }

    /**
     * @return mixed
     */
    public function getLibTarif()
    {
        return $this->libTarif;
    }

    /**
     * @param mixed $libTarif
     */
    public function setLibTarif($libTarif): void
    {
        $this->libTarif = $libTarif;
    }

    /**
     * @return mixed
     */
    public function getMontantTarif()
    {
        return $this->montantTarif;
    }

    /**
     * @param mixed $montantTarif
     */
    public function setMontantTarif($montantTarif): void
    {
        $this->montantTarif = $montantTarif;
    }




}
