<?php
class TarifAbonnement{
    use hydrate;

   private $id;
   private $idTarif;
   private $idAbo;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdTarif()
    {
        return $this->idTarif;
    }

    /**
     * @param mixed $idTarif
     */
    public function setIdTarif($idTarif): void
    {
        $this->idTarif = $idTarif;
    }

    /**
     * @return mixed
     */
    public function getIdAbo()
    {
        return $this->idAbo;
    }

    /**
     * @param mixed $idAbo
     */
    public function setIdAbo($idAbo): void
    {
        $this->idAbo = $idAbo;
    }
}
