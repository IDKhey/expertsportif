<?php
class Utilisateur
{

    use hydrate;

    private $IDUtilisateur;
    private $NomU;
    private $PrenomU;
    private $AdresseU;
    private $VilleU;
    private $DateU;
    private $StatutU;
    private $MailU;
    private $MdpU;
    private $TelU;

    /**
     * @return mixed
     */
    public function getIDUtilisateur()
    {
        return $this->IDUtilisateur;
    }

    /**
     * @param mixed $IDUtilisateur
     */
    public function setIDUtilisateur($IDUtilisateur): void
    {
        $this->IDUtilisateur = $IDUtilisateur;
    }

    /**
     * @return mixed
     */
    public function getNomU()
    {
        return $this->NomU;
    }

    /**
     * @param mixed $NomU
     */
    public function setNomU($NomU): void
    {
        $this->NomU = $NomU;
    }

    /**
     * @return mixed
     */
    public function getPrenomU()
    {
        return $this->PrenomU;
    }

    /**
     * @param mixed $PrenomU
     */
    public function setPrenomU($PrenomU): void
    {
        $this->PrenomU = $PrenomU;
    }

    /**
     * @return mixed
     */
    public function getAdresseU()
    {
        return $this->AdresseU;
    }

    /**
     * @param mixed $AdresseU
     */
    public function setAdresseU($AdresseU): void
    {
        $this->AdresseU = $AdresseU;
    }

    /**
     * @return mixed
     */
    public function getVilleU()
    {
        return $this->VilleU;
    }

    /**
     * @param mixed $VilleU
     */
    public function setVilleU($VilleU): void
    {
        $this->VilleU = $VilleU;
    }

    /**
     * @return mixed
     */
    public function getDateU()
    {
        return $this->DateU;
    }

    /**
     * @param mixed $DateU
     */
    public function setDateU($DateU): void
    {
        $this->DateU = $DateU;
    }

    /**
     * @return mixed
     */
    public function getStatutU()
    {
        return $this->StatutU;
    }

    /**
     * @param mixed $StatutU
     */
    public function setStatutU($StatutU): void
    {
        $this->StatutU = $StatutU;
    }

    /**
     * @return mixed
     */
    public function getMailU()
    {
        return $this->MailU;
    }

    /**
     * @param mixed $MailU
     */
    public function setMailU($MailU): void
    {
        $this->MailU = $MailU;
    }

    /**
     * @return mixed
     */
    public function getMdpU()
    {
        return $this->MdpU;
    }

    /**
     * @param mixed $MdpU
     */
    public function setMdpU($MdpU): void
    {
        $this->MdpU = $MdpU;
    }


    /**
     * @return mixed
     */
    public function getTelU()
    {
        return $this->TelU;
    }

    /**
     * @param mixed $TelU
     */
    public function setTelU($TelU): void
    {
        $this->TelU = $TelU;
    }


}


