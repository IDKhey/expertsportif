

Public class Point{
	
	private Double abscisse;
	private doule ordonnee

	public Point(double abscisse, double ordonnee){

	this.abscisse = abscisse
	this.ordonne = ordonnee

	}

	Public void deplacerPoint(double x , double y) {
	This.abscisse = x ;
	This.ordonnee = y ;
	}

	Public double calculdistance (Point unPoint) {
	Double resultat ;
	Resultat = Math.sqrt(Math.pow(unPoint.abscisse – this.abscisse,2) + Math.pow(unPoint.ordonne – this.ordonnee,2)) ;

	return resultat; }



}





Etapes a suivre pour réaliser un test unitaire :

1 - Creer un objet de test (O.U.T)

2 - Appeler la methode à tester (Si necessaire, creer au préalable d'autres objets pour les parametres de la methode)

3 - Verification / Association => Methodes assert (methode qui sert a verifier qqch)





public class TestPoint{

	//Attribut pour stocker l'objet a tester
	private Point lepoint;

	//Methode éxecutée avant chaques test unitaire
	@Before

	public void setUp() throwsExecption {

		this.lepoint = new Point(8,13);

	}

	@Test

	public void testPointNotNull{

		assertNotNull("L'objet n'a pas été crée correctement",this.lepoint);

	}

	@Test

	public void testPointInitialisation(){

		assertTrue("L'abscisse n'a pas été initialiser correctement", this.lepoint.getAbscisse()==8 );
		assertTrue("L'ordonnee n'a pas été initialiser correctement", this.lepoint.getOrdonne()==13 );

	}

	@Test

	public void testDeplacerPoint(){
	
	//Appel de la methode a tester
		assertTrue("L'abscisse n'a pas été initialiser correctement", this.lepoint.getAbscisse()==8 );
		assertTrue("L'ordonnee n'a pas été initialiser correctement", this.lepoint.getOrdonne()==13 );
		this.lepoint.deplacerPoint(66,7);
		assertTrue("L'abscisse n'a pas été initialiser correctement", this.lepoint.getAbscisse()==66 );
		assertTrue("L'ordonnee n'a pas été initialiser correctement", this.lepoint.getOrdonne()==7 );


	}


	public void testCalculDistance(){

		Point unPoint = new Point(9,14);
		double res = this.lepoint.calculdistance(unPoint);
		assertTrue("La distance n'a pas été calculéee correctement",res==Math.sqrt(Math.pow(9-8,2)) + Math.pow(14-13,2));
	}


}

