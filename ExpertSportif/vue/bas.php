<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<footer class="text-center text-white bg-c-blue">


    <!-- Grid container -->
    <div class="container pt-4">
        <!-- Section: Social media -->
        <section class="mb-auto">


            <img src="img/logoES.png" class=" navbar-logo">

            </br>
            <!-- Facebook -->
            <a
                    class="btn btn-link btn-floating btn-lg text-light m-1"
                    href="https://www.facebook.com/Expert-Sportif-108270647328284"
                    role="button"
                    data-mdb-ripple-color="light"
            ><i class="fa fa-facebook-f"></i
                ></a>


            <!-- Instagram -->
            <a
                    class="btn btn-link btn-floating btn-lg text-light m-1"
                    href=https://www.instagram.com/expertsportif/"
                    role="button"
                    data-mdb-ripple-color="light"
            ><i class="fa fa-instagram"></i
                ></a>


           </br>

            © 2021 <a class="text-light" href="https://expertsportif.fr//">Expert Sportif</a>. Tous droits réservés.
        </section>

    </div>


</footer>


