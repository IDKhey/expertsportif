<nav class="navbar navbar-expand-lg navbar-dark bg-c-blue ">
    <div class="container-fluid">
        <img class="navbar-logo" src="img/logoES.png">

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarNavAltMarkup">

            <div class="navbar-nav navbar-center mx-auto " >

                <a class="nav-link " href="index.php?Menu=Accueil">Accueil</a>
                <a class="nav-link" href="index.php?Menu=Abonnement">Formules</a>
                <!-- <a class="nav-link" href="index.php?Menu=Cours">Cours</a> -->
                <?php if(!isset($_SESSION['utilisateur']) ) : ?>
                    <a class="nav-link" href="index.php?Menu=Connexion">Connexion</a>
                <?php  else : ?>
                    <?php if(utilisateurDAO::verifAdmin()) : ?>
                    <a class="nav-link" href="index.php?Menu=ProgPerso">Mes Clients</a>
                    <?php else : ?>
                    <a class="nav-link" href="index.php?Menu=ProgPerso">Mes Programmes</a>
                    <?php endif; ?>
                    <a class="nav-link" href="index.php?Menu=MonCompte">Mon compte</a>
                    <a class="nav-link" href="index.php?Menu=Deconnexion">Déconnexion</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</nav>

