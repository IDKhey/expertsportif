<div class="conteneur">
    <header>
        <?php include 'haut.php' ;?>
    </header>

    <main class="container">

        <div class=""></div>





        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h3 class="display-5"> REJOINS LA TEAM EXPERT SPORTIF : </h3>

        </div>


        <div class="row d-inline-flex">
            <?php foreach($abonnements as $abo) :?>

                <!-- Gallery Item 1 -->
                <div class ="col-4 p-2">
                <div class="card mb-4 h-100 mx-auto ">
                    <div class="card-header text-light bg-c-blue">
                        <h4 class="my-0 fw-normal"><?php echo $abo->getLibAbo(); ?></h4>
                    </div>
                    <div class="card-body text-center">
                        <ul class="list-unstyled mt-3 mb-4">
                            <li><?php echo $abo->getDescriptifAbo(); ?></li>
                        </ul>

                        <?php foreach($abo->tarifs as $tarif) :?>

                            <p class="card-title pricing-card-title"><small class="text-muted">

                                    <?php echo $tarif['libTarif']; ?> &nbsp;:  <?php  echo $tarif['montantTarif']; ?> € / séance</small></p>
                        <?php endforeach; ?>

                    </div>

                    <div class="card-footer">
                        <button onclick="location.href='https://www.instagram.com/hb_es_performance_/'" type="button" class="w-100 btn btn-lg btn-dark">Contactez-moi !</button>
                    </div>
                </div>
                </div>
            <?php endforeach; ?>
        </div>

</div>
</div>
</main>
<footer>
    <?php include 'bas.php';?>
</footer>
</div>


