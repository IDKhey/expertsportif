<header>
<?php include 'haut.php' ;?>
</header>


<div id="carouselPresentation" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselPresentation" data-slide-to="0" class="active"></li>
        <li data-target="#carouselPresentation" data-slide-to="1"></li>
        <li data-target="#carouselPresentation" data-slide-to="2"></li>
        <li data-target="#carouselPresentation" data-slide-to="3"></li>

    </ol>
    <div class="carousel-inner">


        <div class="carousel-item active" data-interval="5000">
            <img src="img/Experts_1555.jpg" class="d-block w-100 img-fluid" alt="...">
            <div class="carousel-caption d-block FontCreuse">
                <h1><b>💥 PÔLE PERFORMANCE 💥</b></h1>
            </div>
        </div>

        <div class="carousel-item" data-interval="5000">
            <img src="img/Experts_0497.jpg" class="d-block w-100 img-fluid" alt="...">
            <div class="carousel-caption d-block FontCreuse">
                <h1><b>💥 PRÉPARATION PHYSIQUE - <br> MENTALE & COGNITIVE 💥</br></b></h1>
            </div>
        </div>

        <div class="carousel-item" data-interval="5000">
            <img src="img/Experts_1505.jpg" class="d-block w-100 img-fluid" alt="...">
            <div class="carousel-caption d-block FontCreuse">
                <h1><b>⚔ FIGHTER PERFORMANCE TRAINING ⚔</b></h1>

            </div>
        </div>


            <div class="carousel-item" data-interval="5000">
                <img src="img/Experts_2814.jpg" class="d-block w-100 img-fluid" alt="...">
                <div class="carousel-caption d-block FontCreuse">
                    <h1><b>⚔ TEAM EXPERT SPORTIF ⚔</b></h1>

                </div>
            </div>
        </div>
    <div>
        <a class="carousel-control-prev" href="#carouselPresentation" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselPresentation" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>
<div class="container-fluid bg-c-blue text-center text-light">
    </br>
    </br>

    <div class='my-3 fw-normal '>
        <h1 class="FontCreuse">💥 PÔLE PERFORMANCE 💥</h1>

        </br>
        </br>

        <h5>1️⃣ Retrouve ton pôle performance au sein de ton studio de coaching <a class="text-light" href="https://expertsportif.fr//">Expert Sportif</a>. </br>

            2️⃣ Des services complets alliants préparation physique, cognitive et mentale pour te faire progresser ! </br>

            3️⃣ Nous t’accueillons, athlète professionnel, pour vivre l'expérience d'une préparation complète faite sur mesure.</br></br>


            Atteins tes objectifs dans les meilleures conditions existantes.</br>
            Grâce à nos technologies à la pointe de l'innovation, deviens encore meilleur ! </br>
        </h5>

    </div>

    </br>
    </br>

    <div class='fw-normal'>
        <h1 class="FontCreuse">💥 PRÉPARATION PHYSIQUE - MENTALE & COGNITIVE 💥</h1>
        </br>
        </br>
        <div class="embed-responsive w-xl-50 w-lg-50 mx-auto embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="img/PrepaCognitif.mp4" allowfullscreen></iframe>
        </div>
        </br>
        </br>
        </br>

        <h5>🥇 Des offres pour tous, amateurs et professionnels. </br>

            🥇 Coaching individuel & collectif. </br>

            🥇Des technologies aux services de la performance.</br>

            🥇Un studio de 200m2 équipé du matériel le plus qualitatif du marché.</h5>



    </div>

    </br>
    </br>



    <div class='my-3 mb-auto fw-normal'>
        <h1 class="FontCreuse">⚔ FIGHTER PERFORMANCE TRAINING ⚔ </h1>
        </br>
        </br>


        <h5>1️⃣ La préparation physique du combattant accessible à chaque professionnel. </br>

            2️⃣ Du contenu spécifique, adapté aux besoins du combattants.</br>

            3️⃣ Un nombre de participants limité pour un coaching efficace et une pratique securitaire. </br>

        </h5>

        </br>
        </br>
        </br>

    </div>


</div>
<main class="container-fluid">
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center ">
        <h2 class="display-5"> Mets toutes les chances de ton coté ! </h2> </br>
        <h4 class="display-5"> REJOINS LA TEAM EXPERT SPORTIF : </h4>




    </div>


    <div class="card-deck">
        <?php foreach($abonnements as $abo) :?>
            <div class="card mb-4 mx-5 shadow-sm">
                <div class="card-header text-light bg-c-blue">
                    <h4 class="my-0 fw-normal"><?php echo $abo->getLibAbo(); ?></h4>
                </div>
                <div class="card-body  text-center">
                    <ul class="list-unstyled mt-3 mb-4">
                        <li><?php echo $abo->getDescriptifAbo(); ?></li>
                    </ul>

                    <?php foreach($abo->tarifs as $tarif) :?>

                        <p class="card-title pricing-card-title"><small class="text-muted">

                                <?php echo $tarif['libTarif']; ?> &nbsp;:  <?php  echo $tarif['montantTarif']; ?> € / séance</small></p>
                    <?php endforeach; ?>

                </div>
                <div class="card-footer">
                    <button onclick="location.href='https://www.instagram.com/hb_es_performance_/'" type="button" class="w-100 btn btn-lg btn-dark">Contactez-moi !</button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    </div>
    </div>
</main>
<footer>
<?php include 'bas.php';?>
</footer>



