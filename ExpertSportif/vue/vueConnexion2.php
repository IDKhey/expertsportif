
<header>
    <?php include 'haut.php' ;?>
</header>

<div class="conteneur">
    <body class="text-center">

    <main class="form-signin">
        <form action="" method="post" class="form-signin">

            <h1 class="h3 mb-3 fw-normal">Connectez vous :</h1>
            <label for="inputEmail" class="visually-hidden">Adresse Email :</label>
            <input type="email" id="login" name= 'login' class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="visually-hidden">Mot de passe :</label>
            <input type="password" id="mdp" name= 'mdp' class="form-control" placeholder="Password" required>

</div>
<input class="w-100 btn btn-lg btn-primary" type="submit" id="submitConnex" name="submitConnex" value="Connexion" >

<p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
</form>
</main>



</body>
</html>
<footer>
    <?php include 'bas.php';?>
</footer>
</div>
