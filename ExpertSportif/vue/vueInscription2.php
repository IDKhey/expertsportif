<div class="conteneur">
    <header>
        <?php include 'haut.php' ;?>
    </header>
    <main>
        </br>
        <div class="contentInscription">
            <div class='inscription'>
                <div class='titre'>Inscription en ligne</div>
                <?php
                $formulaireInscription->afficherFormulaire();
                echo $message;
                ?>
            </div>
        </div>
    </main>
    <footer>
        <?php include 'bas.php' ;?>
    </footer>
</div>
