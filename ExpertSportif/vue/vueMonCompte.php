<div class="conteneur">
    <header>
        <?php include 'haut.php' ;?>
    </header>
    <main>
        <body class="text-center">
                <div class='h1 my-3 fw-normal'>Modification du Compte : </div>
                <div class='h3 mb-3 fw-normal'>Information personnelles : </div>
                <h1><span class="badge rounded-pill bg-danger text-light"><?php  echo $message; ?></span></h1>
                <?php

                    $formulaireMonCompte->afficherFormulaire();
                ?>

                    <div class='h1 mb-3 fw-normal'>Mot de passe : </div>

                <?php
                    $formulaireMonMDP->afficherFormulaire();
                ?>
            </div>
        </div>
    </main>
    <footer>
        <?php include 'bas.php';?>
    </footer>
</div>