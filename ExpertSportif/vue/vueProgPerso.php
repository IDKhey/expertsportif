<div class="conteneur">
    <header>
        <?php include 'haut.php' ;?>
    </header>

    <main class="container">

        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center ">
            <h2 class="display-5"> Mets toutes les chances de ton coté ! </h2> </br>
            <h4 class="display-5"> REJOINS LA TEAM EXPERT SPORTIF : </h4>


        </div>


        <div class="card-deck">
             <?php foreach($programmes as $prog) :?>
                <div class="card mb-4 mx-5 shadow-sm">
                    <div class="card-header text-light bg-c-blue">
                        <h4 class="my-0 fw-normal"><h><?php echo $prog[0]->getNomProg(); ?></h></h4>
                    </div>

                    <div class="card-footer">
                        <button onclick="location.href='<?php echo $prog[0]->getLienProg(); ?>'" type="button" class="w-100 btn btn-lg btn-dark">Ouvrir</button>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

</div>
</div>

</main>
















<footer>
    <?php include 'bas.php';?>
</footer>
</div>

